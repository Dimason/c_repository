// Task5.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include "LinkedHashMap.h"

using namespace std;

int main()
{
	LinkedHashMap<int, char> map;
	LinkedHashMap<int, char>::LinkedHashMapIterator it(map);
	map.add(5, 'a');
	map.add(3, 'b');

	while (!it.finish()) {
		cout << it.getElement();
		it.next();
	}

	cout << map.get(3) << map.get(5) << endl;

	map.clear();


	map.add(2, 'c');

	it.start();

	while (!it.finish()) {
		cout << it.getElement();
		it.next();
	}

    return 0;
}

