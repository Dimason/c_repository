#pragma once
#include <functional>

template <typename Key, typename T>
class HashMap {
protected:
	static const int HASH_SIZE = 31;
	struct Node {
		Node* prev;
		Node* next;
		Key key;
		T value;
	};
	
	Node arr[HASH_SIZE];
	int getHash(const Key& key) {
		std::hash<Key> key_hash;
		return ((unsigned int)key_hash(key)) % HASH_SIZE;
	}

public:


	class HashMapIterator {
	private:
		HashMap* map;
		int currentIndex;
		Node* currentNode;

	public:

		HashMapIterator(const HashMap& m) {
			map = &m;
			start();
		}
		void start() {
			currentIndex = 0;
			currentNode = map->arr[currentIndex]->next;
		}
		void next() {
			if (currentNode->next != nullptr) {
				currentNode = currentNode->next;
			}
			else {
				while (!finish() && currentNode->next == nullptr) {
					currentIndex++;
					currentNode = arr[curr]->next;
				}
			}
		}
		T& getElement() {
			return currentNode->value;
		}
		bool finish() {
			return currentIndex >= map->HASH_SIZE;
		}
	};

	HashMap() {
		for (int hash = 0; hash < HASH_SIZE; hash++) {
			arr[hash].next = nullptr;
			arr[hash].prev = nullptr;
		}
	}

	virtual void add(const Key& key, T value) {
		int hash = getHash(key);
		Node* curNode = arr[hash].next;
		while (curNode != nullptr) {
			if (curNode->key == key) {
				curNode->value = value;
				return;
			}
			curNode = curNode->next;
		}
		Node* newNode = new Node();
		newNode->value = value;
		newNode->key = key;
		newNode->prev = &arr[hash];
		newNode->next = arr[hash].next;
		arr[hash].next = newNode;
	}
	virtual void remove(const Key& key) {
		int hash = getHash(key);
		Node* curNode = arr[hash].next;
		while (curNode != nullptr) {
			if (curNode->key == key) {
				curNode->prev->next = curNode->next;
				if (curNode->next != nullptr) {
					curNode->next->prev = curNode->prev;
				}
				delete curNode;
			}
		}
	}
	virtual T* get(const Key& key) {
		int hash = getHash(key);
		Node* curNode = arr[hash].next;
		while (curNode != nullptr) {
			if (curNode->key == key) {
				return &curNode->value;
			}
			curNode = curNode->next;
		}
		return nullptr;
	}
	virtual void clear() {
		Node* curNode;
		for (int hash = 0; hash < HASH_SIZE; hash++) {
			while (arr[hash].next != nullptr) {
				curNode = arr[hash].next;
				arr[hash].next = arr[hash].next->next;
				delete curNode;
			}
		}
	}
	virtual bool isEmpty() {
		for (int i = 0; i < HASH_SIZE; i++) {
			if (arr[i].next != nullptr) {
				return false;
			}
		}
		return true;
	}

};

