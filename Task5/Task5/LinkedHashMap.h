#pragma once
#include "HashMap.h"
#include <vector>

using namespace std;

template <typename Key, typename T>
class LinkedHashMap : public HashMap<Key, T> {
private:

	vector<Node*> order;

public:
	class LinkedHashMapIterator{
	private:
		const LinkedHashMap<Key, T> * map;
		int index;

	public:

		LinkedHashMapIterator(const LinkedHashMap& m) {
			map = &m;
			start();
		}
		void start() {
			index = 0;
		}
		void next() {
			index++;
		}
		T& getElement() {
			return map->order[index]->value;
		}
		bool finish() {
			return index == map->order.size();
		}
	};


	LinkedHashMap() : HashMap() {
	}

	virtual void add(const Key& key, T value) {
		int hash = getHash(key);
		Node* curNode = arr[hash].next;
		while (curNode != nullptr) {
			if (curNode->key == key) {
				curNode->value = value;
				for (int i = 0; i < order.size(); i++) {
					if (order[i]->key == key) {
						order.erase(order.begin() + i);
						break;
					}
				}
				order.push_back(curNode);
				return;
			}
			curNode = curNode->next;
		}
		Node* newNode = new Node();
		newNode->value = value;
		newNode->key = key;
		newNode->prev = &arr[hash];
		newNode->next = arr[hash].next;
		arr[hash].next = newNode;
		order.push_back(newNode);
	}
	virtual void remove(const Key& key) {
		int hash = getHash(key);
		Node* curNode = arr[hash].next;
		while (curNode != nullptr) {
			if (curNode->key == key) {
				curNode->prev->next = curNode->next;
				if (curNode->next != nullptr) {
					curNode->next->prev = curNode->prev;
				}
				for (int i = 0; i < order.size(); i++) {
					if (order[i]->key == key) {
						order.erase(order.begin() + i);
						break;
					}
				}
				delete curNode;
			}
		}
	}
	virtual void clear() {
		Node* curNode;
		for (int hash = 0; hash < HASH_SIZE; hash++) {
			while (arr[hash].next != nullptr) {
				curNode = arr[hash].next;
				arr[hash].next = arr[hash].next->next;
				delete curNode;
			}
		}
		order.clear();
	}

};