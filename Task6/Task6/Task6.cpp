// Task6.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "Tree.h"

using namespace std;

int main()
{
	

	Tree* t = new Tree();

	vector<bool>* v = new vector<bool>;

	for (int i = 0; i < 5; i++) {
		t->add(i, *v);
		v->push_back(0);
	}
	delete v;
	v = new vector<bool>;
	v->push_back(1);
	t->add(6, *v);

	cout << *t;

	
	Tree t1 = *t;

	cout << t1.countEven();

    return 0;
}

