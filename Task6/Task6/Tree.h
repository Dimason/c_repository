#pragma once
#include <string>
#include <vector>
#include <iostream>


using namespace std;
class Tree {

private:

	struct Node {
		int value;
		Node* left;
		Node* right;
		Node(int val, Node* left = nullptr, Node* right = nullptr) : value(val), left(left), right(right) {}
	};

	Node* root;
	int size;

	void removeBranch(Node*& root) {
		if (!root) {
			return;
		}
		removeBranch(root->left);
		removeBranch(root->right);
		Node* pointer = root;
		root = nullptr;
		delete pointer;
		size--;
	}

	int countEven(Node*& root) {
		if (!root) {
			return 0;
		}
		return ((root->value + 1) % 2) + countEven(root->left) + countEven(root->right);
	}

	bool containsOnlyPositive(Node*& root) {
		if (!root) {
			return true;
		}
		return root->value > 0 && containsOnlyPositive(root->left) && containsOnlyPositive(root->right);
	}

	void deleteLeaves(Node*& root) {
		if (!root) {
			return;
		}
		if (!root->left && !root->right) {
			delete root;
			root = nullptr;
			size--;
			return;
		}
		deleteLeaves(root->left);
		deleteLeaves(root->right);
	}

	int getSum(Node*& root) {
		if (!root) {
			return 0;
		}
		return root->value + getSum(root->left) + getSum(root->right);
	}

	vector<bool>* find(int subject, Node*& root) {
		if (!root) {
			return nullptr;
		}
		if (root->value == subject) {
			return new vector<bool>;
		}
		vector<bool>* way = find(subject, root->left);
		if (way) {
			way->push_back(0);
			return way;
		}
		way = find(subject, root->right);
		if (way) {
			way->push_back(1);
			return way;
		}
		return nullptr;
	}

	
	bool isAllLess(Node*& root, int x) {
		if (!root) {
			return true;
		}
		return root->value < x && isAllLess(root->left, x) && isAllLess(root->right, x);
	}

	bool isAllMore(Node*& root, int x) {
		if (!root) {
			return true;
		}
		return root->value > x && isAllMore(root->left, x) && isAllMore(root->right, x);
	}

	bool nodeIsBinary(Node*& root) {
		if (!root) {
			return true;
		}
		return nodeIsBinary(root->left) && nodeIsBinary(root->right) &&
			isAllLess(root->left, root->value) && isAllMore(root->right, root->value);
	}

	void writeNode(ostream& out, Node* root, int layer) {
		if (!root) {
			return;
		}
		for (int i = 0; i < layer; i++) {
			out << "    ";
		}
		out << root->value << endl;

		writeNode(out, root->left, layer + 1);
		writeNode(out, root->right, layer + 1);
	}

	Node* copy(Node*& src) {
		if (!src) {
			return nullptr;
		}
		return new Node(src->value, copy(src->left), copy(src->right));
	}

public:

	struct Exception {
		string message;
		Exception(string mes) : message(mes) {}
	};

	Tree() {
		size = 0;
	}
	~Tree() {
		removeBranch(root);
	}

	Tree(Tree& prot) {
		root = copy(prot.root);
		size = prot.size;
	}

	Tree(Tree&& prot) {
		root = prot.root;
		size = prot.size;
		prot.root = nullptr;
		prot.size = 0;
	}

	Tree& operator=(Tree& prot) {
		if (this == &prot) {
			return *this;
		}
		removeBranch(root);
		root = copy(prot.root);
		size = prot.size;
	}

	Tree& operator=(Tree&& prot) {
		if (this == &prot) {
			return *this;
		}
		removeBranch(root);
		root = prot.root;
		size = prot.size;
		prot.root = nullptr;
		prot.size = 0;
	}
	
	void add(int value, vector<bool>& v) {
		Node** tmpNode = &root;
		for (int i = 0; i < v.size(); i++) {
			if (!tmpNode) {
				throw Exception("Wrong path");
			}
			if (v[i] == false) {
				tmpNode = &((*tmpNode)->left);
			}
			else {
				tmpNode = &((*tmpNode)->right);
			}
		}
		Node*& curNode = *tmpNode;
		if (!curNode) {
			curNode = new Node(value);
			size++;
		}
		else {
			curNode->value = value;
		}
	}

	int countEven() {
		return countEven(root);
	}

	bool containsOnlyPositive() {
		return containsOnlyPositive(root);
	}

	void deleteLeaves() {
		deleteLeaves(root);
	}
	
	double getAverage() {
		if (!root) {
			throw Exception("Tree is empty");
		}
		return (double)getSum(root) / size;
	}

	vector<bool>* find(int subject) {
		vector<bool>* ret = find(subject, root);
		if (ret) {
			for (int i = 0; i < ret->size(); i++) {
				swap((*ret)[i], (*ret)[ret->size() - i]);
			}
		}
		return ret;
	}

	bool isBinary() {
		return nodeIsBinary(root);
	}

	friend ostream& operator<<(ostream& out, Tree& t);

};

ostream& operator<<(ostream& out, Tree& t) {
	t.writeNode(out, t.root, 0);
	return out;
}