﻿// Assembler3-2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

/*Вычислить  значение  функции f, которая указана в задании, в  точке  x,
используя  разложение в ряд Тейлора.Суммирование выполнять до тех пор, пока
общий член ряда не будет меньше, по модулю, заданного параметра epsilon.
Решение оформить в виде функции
double имя_функции(double x, double epsilon = 1e-5);

2.   sin(x)*/


double sinFunc(double x, double epsilon = 1e-5) {
    double ret = x;
    int curDel = 1;
    int curNum = 1;
    int curSgn = 0;
    double curElem = x;

    while (curElem > epsilon || curElem < -epsilon) {
        curSgn++;
        curDel = 1;
        curNum++;
        curDel *= curNum;
        curNum++;
        curDel *= curNum;
        curElem *= x;
		curElem *= x;
        curElem /= curDel;
        if (curSgn % 2 == 0) {
            ret += curElem;
        } else {
            ret -= curElem;
        }
    }

    return ret;

}


double sinA(double x, double epsilon = 1e-5) {

	__asm {
		finit;
		; Округление;
		fld		qword ptr x;
		fldpi;
		mov		ebx, 2; Начало дичи;
		push	ebx;
		fild	[esp];
		pop		ebx;
		fmulp	st(1), st(0); Конец дичи;
		fdivp	st(1), st(0);
		fld		st(0);
		frndint;
		fsubp	st(1), st(0);
		fldpi;
		mov		ebx, 2; Начало дичи;
		push	ebx;
		fild	[esp];
		pop		ebx;
		fmulp	st(1), st(0); Конец дичи;
		fmulp	st(1), st(0);
		fstp	x;


		mov		ebx, 1; текущий делитель;
		mov		ecx, 1; текущий n;
		mov		esi, 0; Если 0, то + , иначе - ;
		fld		x; На дне - это ответ будет;
		fld		epsilon; Посерединке - это эпсилон будет;
		fld		x; Наверху - это текущий элемент будет;

		; Цикл;
	CYCLE:
		fld		st(0);
		fabs;
		fcomp	st(2);
		fstsw	ax;
		sahf;
		jc		END;

		not		esi;
		mov		ebx, 1;
		mov		eax, ebx;
		inc		ecx;
		imul	ecx;
		inc		ecx;
		imul	ecx;
		mov		ebx, eax;
		fld		qword ptr x;
		fmul	st(1), st(0);
		fmulp	st(1), st(0);
		push	ebx;
		fild	[esp];
		add		esp, 4;
		fdivp	st(1), st(0);
		cmp		esi, 0;
		jne		SUBSTRACT;
		fadd	st(2), st(0);
		jmp		NEXT_CYCLE;
	SUBSTRACT:
		fsub	st(2), st(0)
	NEXT_CYCLE:
		jmp CYCLE;
	END:
		fstp	x;
		fstp	x;
	}

}


int main()
{
	double param = 3.1415926;


    while (true)
    {
        std::cout << "Enter x: ";
        std::cin >> param;
        std::cout << "Sin x = " << sinA(param) << " " << fabs(sin(param) - sinA(param)) << '\n';
        std::cin.clear();
        fflush(stdin);
    }
}
