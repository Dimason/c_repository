// Assembler1-2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <stdio.h>
#include <cstdlib>
#include <string.h>

// 2.    Выделить  из  строки  подстроку указанной  длины,  начиная  с  заданной позиции.

char* getSubstringInC(char* str, unsigned int substrStart, unsigned int substrLen) {
	//1 - Get string length
	unsigned int strLen = 0;
	while (str[strLen] != '\0') {
		strLen++;
	}
	//2 - Check that params are correct
	if (substrStart + substrLen > strLen) {
		return nullptr;
	}
	//3 - Allocate memory
	char* result = new char[strLen + 1];
	//4 - Fill result
	for (unsigned int i = 0; i < substrLen; i++) {
		result[i] = str[substrStart + i];
	}
	result[substrStart + substrLen] = '\0';
	//5 - Return result
	return result;
}


char* getSubstring(char* mainStr, unsigned int substrStart, unsigned int substrLen) {

    __asm {
        ; 1 - Get string length;
        mov		eax, mainStr;
        mov		ebx, 0;
    INC_LEN:
        cmp     byte ptr [eax][ebx], 0;
        je		LEN_FOUND;
        inc		ebx;
        jmp		INC_LEN;
    LEN_FOUND:

        ; 2 - Check that params are correct;
        mov		edx, substrStart;
        add		edx, substrLen;
        cmp		edx, ebx;
        jg		BAD_ENDING;
        
        ; 3 - Allocate memory;
        push	ebx;
        mov		esi, substrLen;
        inc		esi;
        push	esi;
        call	malloc;
        add		esp, 4;
        pop		ebx;

        ; 4 - Fill result;
        mov		ecx, substrLen;
        mov		edi, mainStr;
        mov		esi, substrStart;
        lea		edi, [edi][esi];
        mov		byte ptr[eax][ecx], 0;
        jecxz   TRUE_ENDING;
    CYCLE:
        mov		dl, [edi][ecx - 1];
        mov     [eax][ecx - 1], dl;
        loop	CYCLE;
        jmp		TRUE_ENDING;
    BAD_ENDING:
        mov		eax, 0;
    TRUE_ENDING:

        ; 5 - Return result;
    }
}




int main()
{
	const unsigned int maxLen = 100;
	char str[maxLen];
	unsigned int substrStart, substrLen;

	gets_s(str);
	scanf_s("%i %i", &substrStart, &substrLen);


	char *result = getSubstring(str, substrStart, substrLen);
	if (!result) {
		printf("Wrong params!");
	}
	else {
		printf("%s", result);
	}
	

}

