#include "stdafx.h"
#include "Victor.h"


Victor::Victor()
{
	this->size = 0;
	this->res = 0;
}

Victor::Victor(int size)
{
	this->size = size;
	this->res = 0;

	arr = new int[size];
	for (int i = 0; i < size; i++) {
		arr[i] = 0;
	}
}

Victor::Victor(int size, int n)
{
	this->size = size;
	this->res = 0;

	arr = new int[size];
	for (int i = 0; i < size; i++) {
		arr[i] = n;
	}
}

Victor::Victor(int size, int n, int reserve)
{
	this->size = size;
	this->res = reserve;

	arr = new int[size + reserve];
	for (int i = 0; i < size; i++) {
		arr[i] = n;
	}
}

Victor::Victor(Victor & prot)
{
	size = prot.getSize();
	res = prot.getCapacity();

	arr = new int[size + res];
	for (int i = 0; i < size; i++) {
		arr[i] = prot[i];
	}
}

Victor::Victor(Victor&& prot)
{
	size = prot.size;
	res = prot.res;
	arr = prot.arr;
	prot.size = 0;
	prot.res = 0;
	prot.arr = nullptr;
}


Victor::~Victor()
{
	delete[] arr;
}

int Victor::getSize()
{
	return size;
}

int & Victor::operator[](int i)
{
	if (i < 0 || i >= size) {
		throw VictorException("Index out of bounds");
	}
	return arr[i];
}

void Victor::resize(int newSize)
{
	if (size <= 0) {
		throw VictorException("Size must be positive");
	}
	if (newSize <= size + res) {
		int oldSize = size;
		res += size - newSize;
		size = newSize;
		for (int i = oldSize; i < size; i++) {
			arr[i] = 0;
		}
	}
	else {
		int* newArr = new int[newSize];
		res = 0;
		for (int i = 0; i < size; i++) {
			newArr[i] = arr[i];
		}
	}
}

void Victor::reserve(int n)
{
	if (n < 0) {
		throw VictorException("Reserve must not be negative");
	}
	int* newArr = new int[size + n];
	res = n;
	for (int i = 0; i < size; i++) {
		newArr[i] = arr[i];
	}
	delete[] arr;
	arr = newArr;
}

int Victor::getCapacity()
{
	return res;
}

void Victor::pushBack(int elem)
{
	if (res == 0) {
		throw VictorException("Array is full");
	}
	res--;
	size++;
	arr[size - 1] = elem;
}

int Victor::popBack()
{
	if (size == 0) {
		throw VictorException("Nothing to pop back");
	}
	int ret = arr[size - 1];
	size--;
	res++;
	return ret;
}

Victor & Victor::operator=(Victor & prot)
{
	if (prot == *this) {
		return *this;
	}

	delete[] arr;

	size = prot.getSize();
	res = prot.getCapacity();

	arr = new int[size + res];
	for (int i = 0; i < size; i++) {
		arr[i] = prot[i];
	}
	return *this;
}

Victor & Victor::operator=(Victor && prot)
{
	if (prot == *this) {
		return *this;
	}

	delete[] arr;

	size = prot.size;
	res = prot.res;
	arr = prot.arr;
	prot.size = 0;
	prot.res = 0;
	prot.arr = nullptr;
	return *this;
}


bool operator==(Victor& v1, Victor& v2)
{
	if (v1.getSize() != v2.getSize()) {
		throw VictorException("Sizes dont match");
	}
	for (int i = 0; i < v1.getSize(); i++) {
		if (v1[i] != v2[i]) {
			return false;
		}
	}
	return true;
}

bool operator!=(Victor & v1, Victor & v2)
{
	return !(v1 == v2);
}

bool operator<(Victor & v1, Victor & v2)
{
	int minSize = v1.getSize() < v2.getSize() ? v1.getSize() : v2.getSize();

	for (int i = 0; i < minSize; i++) {
		if (v1[i] < v2[i]) {
			return true;
		}
		if (v1[i] > v2[i]) {
			return false;
		}
	}

	return v1.getSize() < v2.getSize();
}

bool operator<=(Victor & v1, Victor & v2)
{
	int minSize = v1.getSize() < v2.getSize() ? v1.getSize() : v2.getSize();

	for (int i = 0; i < minSize; i++) {
		if (v1[i] < v2[i]) {
			return true;
		}
		if (v1[i] > v2[i]) {
			return false;
		}
	}

	return v1.getSize() <= v2.getSize();
}

bool operator>(Victor & v1, Victor & v2)
{
	return !(v1 <= v2);
}

bool operator>=(Victor & v1, Victor & v2)
{
	return !(v1 < v2);
}

Victor operator+(Victor v1, Victor v2)
{
	Victor v(v1);
	v.reserve(v2.getSize());
	for (int i = 0; i < v2.getSize(); i++) {
		v.pushBack(v2[i]);
	}
	return v;
}

std::ostream & operator<<(std::ostream & out, Victor & v)
{
	out << v.getSize();
	for (int i = 0; i < v.getSize(); i++) {
		out << " " << v[i];
	}
	return out;
}

std::istream & operator>>(std::istream & in, Victor & v)
{
	int size;
	in >> size;
	v.resize(size);
	for (int i = 0; i < v.getSize(); i++) {
		in >> v[i];
	}
	return in;
}
