#pragma once
#include <iostream>
#include <string>
struct VictorException {
	std::string message;
	VictorException(std::string mes) {
		message = mes;
	}
};

class Victor
{
private:
	int* arr;
	int size;
	int res;

public:
	const int DEFAULT_SIZE = 10;
	Victor();
	Victor(int size);
	Victor(int size, int n);
	Victor(int size, int n, int reserve);
	Victor(Victor& prot);
	Victor(Victor&& prot);
	~Victor();

	int getSize();
	int& operator[](int i);
	void resize(int newSize);

	void reserve(int n);
	int getCapacity();
	void pushBack(int elem);
	int popBack();

	Victor& operator=(Victor& prot);
	Victor& operator=(Victor&& prot);

};



bool operator== (Victor& v1, Victor& v2);
bool operator!= (Victor& v1, Victor& v2);

bool operator< (Victor& v1, Victor& v2);
bool operator<= (Victor& v1, Victor& v2);
bool operator> (Victor& v1, Victor& v2);
bool operator>= (Victor& v1, Victor& v2);

Victor operator+ (Victor v1, Victor v2);

std::ostream& operator<< (std::ostream& out, Victor& v);
std::istream& operator>> (std::istream& in, Victor& v);
