#pragma once
#include "RingBuffer.h"
class Iterator
{
private:
	RingBuffer* queue;
	int number;
public:
	Iterator(RingBuffer& q);
	~Iterator();

	void start();
	void next();
	bool finish();
	int getValue();
};

