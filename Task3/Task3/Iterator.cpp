#include "stdafx.h"
#include "Iterator.h"



Iterator::Iterator(RingBuffer& q)
{
	queue = &q;
}


Iterator::~Iterator()
{
}

void Iterator::start()
{
	number = 0;
}

void Iterator::next()
{
	number++;
}

bool Iterator::finish()
{
	return number == queue->size;
}

int Iterator::getValue()
{
	return queue->arr[(queue->startIndex + number) % queue->maxSize];
}
