#pragma once
#include <iostream>
struct RingBufferException
{
	std::string message;
	RingBufferException(std::string m) {
		message = m;
	}
};

class RingBuffer
{
private:
	int* arr;
	int maxSize;
	int size;
	int startIndex;

public:
	RingBuffer(int n);
	~RingBuffer();

	void push(int elem);
	int pop();
	int peek();
	
	int getSize();
	void clear();
	bool isEmpty();

	friend class Iterator;
};

