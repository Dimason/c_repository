// Task3.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "RingBuffer.h"
#include "Iterator.h"

using namespace std;

int main()
{
	RingBuffer r = RingBuffer(4);

	r.push(1);
	r.push(2);
	r.push(3);
	r.push(4);

	r.pop();
	r.pop();

	r.push(5);
	r.push(6);

	Iterator i = Iterator(r);

	i.start();

	while (!i.finish()) {
		cout << i.getValue() << endl;
		i.next();
	}

	cout << endl;

	while (!r.isEmpty()) {
		cout << r.pop() << endl;
	}

    return 0;
}

