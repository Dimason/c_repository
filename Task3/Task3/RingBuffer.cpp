#include "stdafx.h"
#include "RingBuffer.h"


RingBuffer::RingBuffer(int n)
{
	maxSize = n;
	size = 0;
	arr = new int[n];
}


RingBuffer::~RingBuffer()
{
	delete[] arr;
}

void RingBuffer::push(int elem)
{
	if (size == maxSize) {
		throw RingBufferException("Queue is full");
	}
	arr[(startIndex + size) % maxSize] = elem;
	size++;
}

int RingBuffer::pop()
{
	if (size == 0) {
		throw RingBufferException("Queue is full");
	}
	int ret = arr[startIndex];
	startIndex = (startIndex + 1) % maxSize;
	size--;
	return ret;
}

int RingBuffer::peek()
{
	if (size == 0) {
		throw RingBufferException("Queue is full");
	}
	return arr[startIndex];
}

int RingBuffer::getSize()
{
	return size;
}

void RingBuffer::clear()
{
	size = 0;
}

bool RingBuffer::isEmpty()
{
	return size == 0;
}
