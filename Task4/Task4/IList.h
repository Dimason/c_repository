#pragma once
#include <string>
#include "IIterator.h"
using namespace std;

struct ListException {
	string message;
	ListException(string message) : message(message) {}
};

template <typename T>
class IList
{

public:
	
	//virtual void add(const IIterator& i, int val) = 0;
	//virtual void remove(const IIterator& i) = 0;
	virtual IIterator<T>* find(T elem) = 0;
	virtual void clear() = 0;
	virtual bool isEmpty() = 0;
	virtual int getSize() = 0;
	virtual IIterator<T>* getStart() = 0;



};

