#pragma once
#include "IList.h"

template <typename T>
class List : public IList<T>
{
private:
	struct Node
	{
		Node* next;
		Node* prev;
		T value;
	};

	Node* bufferNode;
	int size;

public:
	class Iterator : public IIterator<T>
	{
	private:
		List<T>* list;
		Node* startNode;
		Node* currentNode;
	public:

		Iterator(List& l, Node& pos) {
			list = &l;
			startNode = &pos;
			start();
		}
		~Iterator() {
		}
		void start() {
			currentNode = startNode;
		}
		T getElement() {
			return currentNode->value;
		}
		void next() {
			currentNode = currentNode->next;
		}
		bool finish() {
			return currentNode == list->bufferNode;
		}

		friend class List;
	};
	
	List() {
		bufferNode = new Node();
		bufferNode->next = bufferNode;
		bufferNode->prev = bufferNode;
	}
	~List() {
		clear();
		delete bufferNode;
	}


	List(List&& l) {
		bufferNode = l.bufferNode;
		l.bufferNode = nullptr;
	}

	void add(const Iterator& i, T val) {
		if (i.list != this) {
			throw ListException("Wrong iterator");
		}
		Node* newNode = new Node();
		newNode->next = i.currentNode;
		newNode->prev = i.currentNode->prev;
		newNode->value = val;
		i.currentNode->prev->next = newNode;
		i.currentNode->prev = newNode;
	}
	void remove(const Iterator& i) {
		if (i.list != this) {
			throw ListException("Wrong iterator");
		}
		if (i.currentNode == bufferNode) {
			return;
		}
		i.currentNode->prev->next = i.currentNode->next;
		i.currentNode->next->prev = i.currentNode->prev;
		delete i.currentNode;
	}
	Iterator* find(string elem) {
		Node* node = bufferNode->next;

		while (node != bufferNode) {
			if (node->value == elem) {
				return new Iterator(*this, *node);
			}
			node = node->next;
		}

		return nullptr;
	}
	void clear() {
		Node* node = bufferNode->prev;

		while (node != bufferNode) {
			node = node->prev;
			delete node->next;
		}

		bufferNode->next = bufferNode;
		bufferNode->prev = bufferNode;

	}
	bool isEmpty() {
		return bufferNode->next == bufferNode;
	}
	int getSize() {
		int size = 0;
		Node* node = bufferNode->next;
		while (node != bufferNode) {
			size++;
			node = node->next;
		}
		return size;
	}
	Iterator* getStart() {
		return new Iterator(*this, *(bufferNode->next));
	}

};


