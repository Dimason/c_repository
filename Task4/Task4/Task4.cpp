// Task4.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include "List.h"

using namespace std;

int main()
{
	List<string> list = List<string>();

	List<string>::Iterator* it = list.getStart();
	it->start();

	for (int i = 0; i < 5; i++) {
		list.add(*it, "a");
	}
	delete it;

	list.remove(*list.getStart());
	list.remove(*list.getStart());


	cout << "Size = " << list.getSize() << endl;


	it = list.getStart();
	it->start();



	while (!it->finish()) {
		cout << it->getElement() << " ";
		it->next();
	}
	
	delete it;

    return 0;
}

