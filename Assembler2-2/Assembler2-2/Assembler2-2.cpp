// Assembler2-2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <stdio.h>
#include <cstdlib>

struct Matrix {
	unsigned int n;
	int** arr;
};

Matrix* createMatrix(unsigned int n) {
	__asm {
		; Выделим память под структуру : 4 байта инт, 4 байта указатель;
		mov		ebx, 8;
		push	ebx;
		call	malloc;
		add		esp, 4;
		mov		esi, eax;

		; Запишем значение n;
		mov		ebx, n;
		mov		[esi], ebx;
		; Выделим память под arr ;
		mov		eax, 4;
		mul		ebx;
		push	esi;
		push	eax;
		call	malloc;
		add		esp, 4;
		pop		esi;
		mov		[esi + 4], eax;
		; Выделим память под собственно матрицу (сразу под всю, чтобы не делать цикл с маллоком);
		mov		ebx, n;
		mov		eax, 4;
		mul		ebx;
		mul		ebx;
		push	esi;
		push	eax;
		call	malloc;
		add		esp, 4;
		pop		esi;
		; И заполним массив указателями куда надо;
		mov		edi, [esi + 4];
		mov		ecx, n;
	CYCLE:
		mov		[edi], eax;
		add		edi, 4;
		add		eax, n; Мне не стыдно за это
		add		eax, n;
		add		eax, n;
		add		eax, n;
		loop CYCLE;

		; Осталось вернуть результат;
		mov eax, esi;
	}
}

int getElem(Matrix* matrix, unsigned int i, unsigned int j) {
	__asm {
		mov		eax, matrix;
		add		eax, 4;
		mov		eax, [eax];

		add		eax, i; Всё ещё не стыдно;
		add		eax, i;
		add		eax, i;
		add		eax, i;
		mov		eax, [eax];

		add		eax, j;
		add		eax, j;
		add		eax, j;
		add		eax, j;
		mov		eax, [eax];
	}
}

void setElem(Matrix* matrix, int value, unsigned int i, unsigned int j) {
	__asm {
		mov		eax, matrix;
		add		eax, 4;
		mov		eax, [eax];

		add		eax, i; Всё ещё не стыдно;
		add		eax, i;
		add		eax, i;
		add		eax, i;
		mov		eax, [eax];

		add		eax, j;
		add		eax, j;
		add		eax, j;
		add		eax, j;
		push	ebx; Сохраняю значение ebx
		mov		ebx, value;
		mov		[eax], ebx;
		pop		ebx;
	}
}

int getDet(Matrix* matrix) {
    __asm {
        ; Проверка на окончание рекурсии - если у матрицы размерность 1;
        mov     ebx, matrix;
        mov     ecx, [ebx];
        cmp     ecx, 1;
        je      SIMPLE_MATRIX;
        
        ; Заранее создадим пустую матрицу на размер меньше;
        mov     eax, [ebx];
        sub     eax, 1;
        push    eax;
		call    createMatrix;
        add     esp, 4;

        mov     esi, eax; Здесь будет вспомогательная матрица размером n - 1;
        mov     edx, 0; Сюда будем записывать определитель;
        mov     ecx, 0; Индекс цикла от 0 до n невключительно;
        mov     ebx, matrix; Храним тут указатель на начало матрицы;
        
        ; Раскладываем матрицу по 0 - му столбцу;

    CYCLE_ROWS:
        cmp     ecx, [ebx];
        je      EXIT_CYCLE_ROWS;
        ; Заполняем указатели вспомогательной матрицы как надо;
		push    edx;
		push    ecx;
        mov     ecx, 0;
        mov     edx, [ebx + 4]; Начало массива указателей большой матрицы;
		mov		edi, [esi + 4]; Текущий элемент массива указателей вспомогательной матрицы;
    CYCLE_ROWS_2:
        cmp     ecx, [ebx];
        je      EXIT_CYCLE_ROWS_2;
        cmp     ecx, [esp];
        je SKIP_CURRENT_ROW;
		mov		eax, [edx][ecx * 4];
		add		eax, 4;
		mov		dword ptr [edi], eax;
		add		edi, 4;
	SKIP_CURRENT_ROW:
		inc		ecx;
        jmp CYCLE_ROWS_2;
	EXIT_CYCLE_ROWS_2:

        ; Считаем определитель вспомогательной матрицы
        push    esi;
        call    getDet;
		pop     esi;
        pop     ecx;

		mov		edx, 1;
		and		edx, ecx;
		cmp		edx, 0; Не хватало регистров, мы выживали как могли
		pop     edx;
		je		NOT_INVERT;
		neg		eax;
	NOT_INVERT:
		mov		ebx, matrix;
		mov     edi, eax; Тут теперь определитель текущей вспомогательной матрицы;
		mov		eax, [ebx + 4];
		mov		eax, [eax][ecx * 4];
		mov		eax, [eax];
		push	edx;
		imul	edi;
		pop		edx;
		add		edx, eax;
        inc     ecx;
        jmp CYCLE_ROWS;
    EXIT_CYCLE_ROWS:
		mov		eax, edx;
		jmp		END;

    SIMPLE_MATRIX:
        mov     eax, matrix;
        mov     eax, [eax + 4];
		mov     eax, [eax];
		mov		eax, [eax];
    END:
		
    }
}

int main()
{
	int n;
	printf("Enter size of matrix\n");
	scanf_s("%i", &n);
	Matrix* m = createMatrix(n);
	printf("Now enter matrix elements\n");
	int x;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			scanf_s("%i", &x);
			setElem(m, x, i, j);
		}
	}
	printf("Determinant is %i\n", getDet(m));

	
    return 0;
}

