#include "pch.h"
#include "Container.h"

namespace BoxesContainers {
	Container::Container(int length, int width, int height, double maxWeight)
	{
		this->length = length;
		this->width = width;
		this->height = height;
		this->maxWeight = maxWeight;
	}

	Container::~Container()
	{

	}

	int Container::getLength()
	{
		return length;
	}

	int Container::getWidth()
	{
		return width;
	}

	int Container::getHeight()
	{
		return height;
	}

	double Container::getMaxWeight()
	{
		return maxWeight;
	}

	int Container::getCount()
	{
		return v.size();
	}

	double Container::getTotalWeight()
	{
		double ret = 0;
		for (Box b : v) {
			ret += b.weight;
		}
		return ret;
	}

	int Container::getTotalValue()
	{
		int ret = 0;
		for (Box b : v) {
			ret += b.value;
		}
		return ret;
	}

	Box& Container::getBox(int index)
	{
		return v.at(index);
	}

	void Container::addBox(Box& item)
	{
		if (getTotalWeight() + item.weight <= maxWeight) {
			v.push_back(item);
		}
		else {
			ContainerException ex;
			throw ex;
		}
	}

	void Container::deleteBox(int index)
	{
		v.erase(v.begin() + index);
	}

	std::ostream& operator<< (std::ostream &out, Container &c) {
		out << "Container(L = " << c.getLength() << ", W = " << c.getWidth()
			<< ", H = " << c.getHeight() << ", max weight = " << c.getMaxWeight() << ")[";
		for (int i = 0; i < c.getCount() - 1; i++) {
			out << c.getBox(i) << ", ";
		}
		if (c.getCount() > 0) {
			out << c.getBox(c.getCount() - 1);
		}
		out << "]";
		return out;
	}

	std::istream& operator>> (std::istream &in, Container &c) {
		int l, w, h;
		double maxW;
		in >> l;
		in >> w;
		in >> h;
		in >> maxW;
		Container ret(l, w, h, maxW);
		c = ret;
		return in;
	}

	Box& Container::operator[] (int index) {
		return v[index];
	}
}




