#pragma once
#include <iostream>
class BoxClass
{
private:
	int length;
	int width;
	int height;
	double weight;
	int value;

public:
	BoxClass(int len = 0, int wid = 0, int hei = 0, double wei = 0, int val = 0);
	~BoxClass();

	int getLength();
	void setLength(int value);

	int getWidth();
	void setWidth(int value);

	int getHeight();
	void setHeight(int value);

	double getWeight();
	void setWeight(int value);

	int getValue();
	void setValue(int val);

	int getVolume();


};

