#include "pch.h"
#include "BoxClass.h"


BoxClass::BoxClass(int len, int wid, int hei, double wei, int val) : length(len), width(wid), height(hei), weight(wei), value(val) {}

BoxClass::~BoxClass()
{
}

int BoxClass::getLength()
{
	return length;
}

void BoxClass::setLength(int value)
{
	length = value;
}

int BoxClass::getWidth()
{
	return width;
}

void BoxClass::setWidth(int value)
{
	width = value;
}

int BoxClass::getHeight()
{
	return height;
}

void BoxClass::setHeight(int value)
{
	height = value;
}

double BoxClass::getWeight()
{
	return weight;
}

void BoxClass::setWeight(int value)
{
	weight = value;
}

int BoxClass::getValue()
{
	return value;
}

void BoxClass::setValue(int val)
{
	value = val;
}

int BoxClass::getVolume() {
	return length * width * height;
}


