#pragma once
#include <vector>
#include "BoxFunctions.h"

namespace BoxesContainers {

	struct ContainerException {};

	class Container
	{
	private:
		std::vector<Box> v;
		int length;
		int width;
		int height;
		double maxWeight;

	public:
		Container(int length, int width, int height, double maxWeight);
		~Container();

		int getLength();
		int getWidth();
		int getHeight();
		double getMaxWeight();

		int getCount();

		double getTotalWeight();

		int getTotalValue();

		Box& getBox(int index);

		void addBox(Box& item);

		void deleteBox(int index);

		Box& operator[] (int index);

	};

	std::ostream& operator<< (std::ostream &out, Container &c);

	std::istream& operator>> (std::istream &in, Container &c);



}

