#include "pch.h"
#include "BoxFunctions.h"

namespace BoxesContainers {
	Box::Box(int len, int wid, int hei, double wei, int val) : length(len), width(wid), height(hei), weight(wei), value(val) {}

	int Box::getVolume() {
		return length * width * height;
	}

	bool operator==(Box &a, Box &b) {
		return a.length == b.length && a.width == b.width && a.height == b.height &&
			a.weight == b.weight && a.value == b.value;
	}

	std::ostream& operator<< (std::ostream &out, Box &b) {
		out << "Box(L = " << b.length << ", W = " << b.width
			<< ", H = " << b.height << ", weight = " << b.weight
			<< ", value = " << b.value << ")";
		return out;
	}

	std::istream& operator>> (std::istream &in, Box &b)
	{
		in >> b.length;
		in >> b.width;
		in >> b.height;
		in >> b.weight;
		in >> b.value;

		return in;
	}


	int getTotalValue(Box* boxes, int size) {
		int ret = 0;
		for (int i = 0; i < size; i++) {
			ret += boxes[i].value;
		}
		return ret;
	}

	bool paramsAreNotTooLarge(Box* boxes, int size, int maxValue) {
		for (int i = 0; i < size; i++) {
			if (boxes[i].length + boxes[i].width + boxes[i].height > maxValue) {
				return false;
			}
		}
		return true;
	}

	double getMaxWeightWhereVolumeNotTooLarge(Box* boxes, int size, int maxVolume) {
		double ret = -1;

		for (int i = 0; i < size; i++) {
			if (boxes[i].getVolume() <= maxVolume && ret < boxes[i].weight) {
				ret = boxes[i].weight;
			}
		}

		return ret;
	}

	bool canBePlacedInEachOther(Box* boxes, int size) {
		//Copy input array
		Box* array = new Box[size];
		for (int i = 0; i < size; i++) {
			array[i] = boxes[i];
		}

		//Bubble sort array by volume
		for (int i = 0; i < size - 1; i++) {
			for (int j = 1; i < size - i; i++) {
				if (array[j - 1].getVolume() > array[j].getVolume()) {
					Box x = array[j - 1];
					array[j - 1] = array[j];
					array[j] = x;
				}
			}
		}
		//Check if every box is nested correctly
		for (int i = 0; i < size - 1; i++) {
			int paramsS[3], paramsL[3];

			paramsS[0] = array[i].height;
			paramsS[1] = array[i].width;
			paramsS[2] = array[i].length;

			paramsL[0] = array[i + 1].height;
			paramsL[1] = array[i + 1].width;
			paramsL[2] = array[i + 1].length;

			//Sort params of small and large boxes

			for (int i = 0; i < 2; i++) {
				for (int j = 1; j < 3; j++) {
					if (paramsS[j - 1] > paramsS[j]) {
						int x = paramsS[j - 1];
						paramsS[j - 1] = paramsS[j];
						paramsS[j] = x;
					}
					if (paramsL[j - 1] > paramsL[j]) {
						int x = paramsL[j - 1];
						paramsL[j - 1] = paramsL[j];
						paramsL[j] = x;
					}
				}
			}

			//If small does not nest in large, return false

			if (paramsS[0] >= paramsL[0] || paramsS[1] >= paramsL[1] || paramsS[2] >= paramsL[2]) {
				return false;
			}

		}

		//Else return true
		return true;

	}
}

