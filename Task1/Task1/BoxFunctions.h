#pragma once
#include <iostream>

namespace BoxesContainers {
	struct Box {
		int length;
		int width;
		int height;
		double weight;
		int value;
		Box(int len = 0, int wid = 0, int hei = 0, double wei = 0, int val = 0);
		int getVolume();
	};


	bool operator==(Box &a, Box &b);

	std::ostream& operator<< (std::ostream &out, Box &b);

	std::istream& operator>> (std::istream &in, Box &b);

	int getTotalValue(Box* boxes, int size);

	bool paramsAreNotTooLarge(Box* boxes, int size, int maxValue);

	double getMaxWeightWhereVolumeNotTooLarge(Box* boxes, int size, int maxVolume);

	bool canBePlacedInEachOther(Box* boxes, int size);
}



