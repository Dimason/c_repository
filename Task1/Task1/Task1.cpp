﻿// Task1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "BoxFunctions.h"
#include "Container.h"



using namespace BoxesContainers;
using namespace std;




void placeInEachOtherTest() {
	Box b1(1, 1, 1, 1, 1), b2(2, 3, 3, 1, 1), b3(4, 4, 3, 1, 1);
	Box boxes[3];
	boxes[0] = b1;
	boxes[1] = b2;
	boxes[2] = b3;

	cout << canBePlacedInEachOther(boxes, 3);

}

void boxInOutTest() {
	Box b;
	cin >> b;

	cout << b;

}

void containerInOutTest() {
	Container c(0, 0, 0, 0);
	cin >> c;
	cout << c;

}


int main()
{
	containerInOutTest();
	
	cout << '\n';
	
}



