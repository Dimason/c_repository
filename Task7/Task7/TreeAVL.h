#pragma once
#include <string>
#include <vector>
#include <iostream>

using namespace std;

struct BadTreeException {
	string message;
	BadTreeException(string message) : message(message) {}
};


class TreeAVL {

private:

	struct Node {
		string value;
		Node* left;
		Node* right;
		int number;
		int balance;
		Node(string val, int balance = 0, Node* left = nullptr, Node* right = nullptr, int number = 1) : value(val), left(left), right(right), number(number), balance(balance) {}
	};

	Node* root;
	int size;

	void removeBranch(Node*& root) {
		if (!root) {
			return;
		}
		removeBranch(root->left);
		removeBranch(root->right);
		Node* pointer = root;
		root = nullptr;
		delete pointer;
		size--;
	}



	int find(string subject, Node*& root) {
		if (!root) {
			return 0;
		}
		if (subject == root->value) {
			return root->number;
		}
		if (subject < root->value) {
			return find(subject, root->left);
		}
		else {
			return find(subject, root->right);
		}
	}



	void writeNode(ostream& out, Node* root) {
		if (!root) {
			return;
		}
		writeNode(out, root->left);
		out << root->value << " : " << root->number << ",  ";
		writeNode(out, root->right);
	}

	Node* copy(Node*& src) {
		if (!src) {
			return nullptr;
		}
		return new Node(src->value, src->balance, copy(src->left), copy(src->right), src->number);
	}

	bool add(string value, Node*& root) {
		if (!root) {
			root = new Node(value);
			return true;
		}
		if (value == root->value) {
			root->number++;
			return false;
		}
		if (value < root->value) {
			if (add(value, root->left)) {
				return balanceAddLeft(root);
			}
			return false;
		}
		else {
			if (add(value, root->right)) {
				return balanceAddRight(root);
			}
			return false;
		}
	}

	bool balanceAddLeft(Node*& root) {
		switch (root->balance) {
		case -1: {
			root->balance = 0;
			return false;
		}
		case 0: {
			root->balance = 1;
			return true;
		}
		case 1: {
			if (root->left->balance > 0) {
				RRotation(root);
				return false;
			}
			if (root->left->balance < 0) {
				LRRotation(root);
				return false;
			}
			
		}
		}
		throw BadTreeException("Tree is not AVL");
	}

	void RRotation(Node*& pA) {
		Node* pB = pA->left;
		pA->balance = pB->balance = 0;
		pA->left = pB->right;
		pB->right = pA;
		pA = pB;
	}

	void LRRotation(Node*& pA) {
		Node* pB = pA->left;
		Node* pC = pB->right;
		if (pC->balance > 0) {
			pA->balance = -1;
			pB->balance = pC->balance = 0;
		}
		else {
			pB->balance = 1;
			pA->balance = pC->balance = 0;
		}
		pA->left = pC->right;
		pB->right = pC->left;
		pC->left = pB;
		pC->right = pA;
		pA = pC;
	}

	bool balanceAddRight(Node*& root) {
		switch (root->balance) {
		case 1: {
			root->balance = 0;
			return false;
		}
		case 0: {
			root->balance = -1;
			return true;
		}
		case -1: {
			if (root->right->balance < 0) {
				LRotation(root);
				return false;
			}
			if (root->right->balance > 0) {
				RLRotation(root);
				return false;
			}
		}
		}
		throw BadTreeException("Tree is not AVL");
	}

	void LRotation(Node*& pA) {
		Node* pB = pA->right;
		pA->balance = pB->balance = 0;
		pA->right = pB->left;
		pB->left = pA;
		pA = pB;
	}

	void RLRotation(Node*& pA) {
		Node* pB = pA->right;
		Node* pC = pB->left;
		if (pC->balance < 0) {
			pA->balance = 1;
			pB->balance = pC->balance = 0;
		}
		else {
			pB->balance = -1;
			pA->balance = pC->balance = 0;
		}
		pA->right = pC->left;
		pB->left = pC->right;
		pC->right = pB;
		pC->left = pA;
		pA = pC;
	}

	int countWords(Node*& root) {
		if (!root) {
			return 0;
		}
		return root->number + countWords(root->left) + countWords(root->right);
	}

	bool del(string value, Node*& root) {
		if (!root) {
			return false;
		}
		if (value == root->value) {
			if (root->number > 1) {
				root->number--;
				return false;
			}
			else {
				Node* pDel = root;
				bool result = true;
				if (!root->right) {
					root = root->left;
				}
				else if (!root->left) {
					root = root->right;
				}
				else if (del2(pDel, root->left)) {
					result = balanceDelLeft(root);
				}
				else {
					result = false;
				}
				delete pDel;
				return result;
			}

		}
		if (value < root->value) {
			if (del(value, root->left)) {
				return balanceDelLeft(root);
			}
			return false;
		}
		else {
			if (del(value, root->right)) {
				return balanceDelRight(root);
			}
			return false;
		}
	}

	bool del2(Node*& root, Node*& pLeft) {
		if (pLeft->right) {
			if (del2(root, pLeft->right)) {
				return balanceDelRight(pLeft);
			}
			return false;
		}
		root->value = pLeft->value;
		root = pLeft;
		pLeft = pLeft->left;
		return true;
	}

	bool balanceDelLeft(Node*& root) {
		switch (root->balance) {
		case 1: {
			root->balance = 0;
			return true;
		}
		case 0: {
			root->balance = -1;
			return false;
		}
		case -1: {
			if (root->right->balance < 0) {
				LRotation(root);
				return true;
			}
			if (root->right->balance > 0) {
				RLRotation(root);
				return true;
			}
			LRotationDel(root);
			return false;
		}
		}
		throw BadTreeException("Tree is not AVL");
	}

	void LRotationDel(Node *&pA)
	{
		Node* pB = pA->right;
		pA->balance = -1;
		pB->balance = 1;
		pA->right = pB->left;
		pB->left = pA;
		pA = pB;
	}

	bool balanceDelRight(Node*& root) {
		switch (root->balance) {
		case -1: {
			root->balance = 0;
			return true;
		}
		case 0: {
			root->balance = 1;
			return false;
		}
		case 1: {
			if (root->left->balance > 0) {
				RRotation(root);
				return true;
			}
			if (root->left->balance < 0) {
				LRRotation(root);
				return true;
			}
			RRotationDel(root);
			return false;
		}
		}
		throw BadTreeException("Tree is not AVL");
	}

	void RRotationDel(Node *&pA)
	{
		Node* pB = pA->left;
		pA->balance = 1;
		pB->balance = -1;
		pA->left = pB->right;
		pB->right = pA;
		pA = pB;
	}

	void writeNodePretty(ostream& out, Node* root, int layer) {
		if (!root) {
			return;
		}
		writeNodePretty(out, root->left, layer + 1);
		for (int i = 0; i < layer; i++) {
			out << "  ";
		}
		out << root->value << ":" << root->number << endl;
		writeNodePretty(out, root->right, layer + 1);
	}

public:

	struct Exception {
		string message;
		Exception(string mes) : message(mes) {}
	};

	TreeAVL() {
		size = 0;
	}
	~TreeAVL() {
		removeBranch(root);
	}

	TreeAVL(TreeAVL& prot) {
		root = copy(prot.root);
		size = prot.size;
	}

	TreeAVL(TreeAVL&& prot) {
		root = prot.root;
		size = prot.size;
		prot.root = nullptr;
		prot.size = 0;
	}

	TreeAVL& operator=(TreeAVL& prot) {
		if (this == &prot) {
			return *this;
		}
		removeBranch(root);
		root = copy(prot.root);
		size = prot.size;
	}

	TreeAVL& operator=(TreeAVL&& prot) {
		if (this == &prot) {
			return *this;
		}
		removeBranch(root);
		root = prot.root;
		size = prot.size;
		prot.root = nullptr;
		prot.size = 0;
	}

	void add(string value) {
		add(value, root);
	}

	void remove(string value) {
		del(value, root);
	}

	int find(string subject) {
		return find(subject, root);
	}

	int countWords() {
		return countWords(root);
	}

	friend ostream& operator<<(ostream& out, TreeAVL& t);

};

ostream& operator<<(ostream& out, TreeAVL& t) {
	//t.writeNode(out, t.root);
	t.writeNodePretty(out, t.root, 0);
	return out;
}