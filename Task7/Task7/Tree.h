#pragma once
#include <string>
#include <vector>
#include <iostream>


using namespace std;
class Tree {

private:

	struct Node {
		string value;
		Node* left;
		Node* right;
		int number;
		Node(string val, Node* left = nullptr, Node* right = nullptr, int number = 1) : value(val), left(left), right(right), number(number) {}
	};

	Node* root;
	int size;

	void removeBranch(Node*& root) {
		if (!root) {
			return;
		}
		removeBranch(root->left);
		removeBranch(root->right);
		Node* pointer = root;
		root = nullptr;
		delete pointer;
		size--;
	}

	

	int find(string subject, Node*& root) {
		if (!root) {
			return 0;
		}
		if (subject == root->value) {
			return root->number;
		}
		if (subject < root->value) {
			return find(subject, root->left);
		}
		else {
			return find(subject, root->right);
		}
	}


	
	void writeNode(ostream& out, Node* root) {
		if (!root) {
			return;
		}
		writeNode(out, root->left);
		out << root->value << " : " << root->number << ",  ";
		writeNode(out, root->right);
	}

	Node* copy(Node*& src) {
		if (!src) {
			return nullptr;
		}
		return new Node(src->value, copy(src->left), copy(src->right), src->number);
	}

	void add(string value, Node*& root) {
		if (!root) {
			root = new Node(value);
			return;
		}
		if (value == root->value) {
			root->number++;
			return;
		}
		if (value < root->value) {
			add(value, root->left);
		}
		else {
			add(value, root->right);
		}
	}

	int countWords(Node*& root) {
		if (!root) {
			return 0;
		}
		return root->number + countWords(root->left) + countWords(root->right);
	}

	void remove(string value, Node*& root) {
		if (!root) {
			return;
		}
		if (value == root->value) {
			if (root->number > 1) {
				root->number--;
			}
			else {
				deleteNode(root);
			}

			return;
		}
		if (value < root->value) {
			remove(value, root->left);
		}
		else {
			remove(value, root->right);
		}
	}

	void deleteNode(Node*& root) {
		if (!root) {
			return;
		}
		if (!root->left && !root->right) {
			delete root;
			root = nullptr;
		}
		else {
			if (!root->left || !root->right) {
				Node* forDelete = root;
				if (root->left) {
					root = root->left;
				}
				else {
					root = root->right;
				}
				delete forDelete;
			}
			else {
				Node** newRoot = &root->left;
				while ((*newRoot)->right) {
					*newRoot = (*newRoot)->right;
				}
				root->value = (*newRoot)->value;
				*newRoot = (*newRoot)->left;
			}
		}

	}


public:

	struct Exception {
		string message;
		Exception(string mes) : message(mes) {}
	};

	Tree() {
		size = 0;
	}
	~Tree() {
		removeBranch(root);
	}

	Tree(Tree& prot) {
		root = copy(prot.root);
		size = prot.size;
	}

	Tree(Tree&& prot) {
		root = prot.root;
		size = prot.size;
		prot.root = nullptr;
		prot.size = 0;
	}

	Tree& operator=(Tree& prot) {
		if (this == &prot) {
			return *this;
		}
		removeBranch(root);
		root = copy(prot.root);
		size = prot.size;
	}

	Tree& operator=(Tree&& prot) {
		if (this == &prot) {
			return *this;
		}
		removeBranch(root);
		root = prot.root;
		size = prot.size;
		prot.root = nullptr;
		prot.size = 0;
	}

	void add(string value) {
		add(value, root);
	}

	void remove(string value) {
		remove(value, root);
	}

	int find(string subject) {
		return find(subject, root);
	}

	int countWords() {
		return countWords(root);
	}

	friend ostream& operator<<(ostream& out, Tree& t);

};

ostream& operator<<(ostream& out, Tree& t) {
	t.writeNode(out, t.root);
	return out;
}